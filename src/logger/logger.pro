TARGET = QtLogger
QT = core
load(qt_module)

PUBLIC_HEADERS += \
    qlogger.h \
    qloggerglobal.h

PRIVATE_HEADERS += \
    qlogger_p.h

SOURCES += \
    qlogger.cpp

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS
