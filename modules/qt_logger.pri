QT.logger.VERSION = 5.0.0
QT.logger.MAJOR_VERSION = 5
QT.logger.MINOR_VERSION = 0
QT.logger.PATCH_VERSION = 0

QT.logger.name = QtLogger
QT.logger.bins = $$QT_MODULE_BIN_BASE
QT.logger.includes = $$QT_MODULE_INCLUDE_BASE $$QT_MODULE_INCLUDE_BASE/QtLogger
QT.logger.private_includes = $$QT_MODULE_INCLUDE_BASE/QtLogger/$$QT.logger.VERSION
QT.logger.sources = $$QT_MODULE_BASE/src/logger
QT.logger.libs = $$QT_MODULE_LIB_BASE
QT.logger.depends = core
QT.logger.DEFINES = QT_LOGGER_LIB

QT_CONFIG += logger
