/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the logger module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QLOGGER_H
#define QLOGGER_H

#include <QtCore/QObject>
#include <QtCore/QDebug>
#include <QtLogger/qloggerglobal.h>

QT_BEGIN_HEADER
QT_LOGGER_BEGIN_NAMESPACE

class QLoggingCategoryPrivate;
class Q_LOGGER_EXPORT QLoggingCategory {
    Q_DISABLE_COPY(QLoggingCategory)
public:
    explicit QLoggingCategory(const char *category);
    ~QLoggingCategory();
    bool isEnabled(QtMsgType msgtype);
    const char * categoryName();
    static QLoggingCategory& defaultCategory();
private:
    QLoggingCategoryPrivate *d_ptr;
    const char *_categoryName;

    friend class QLoggingPrivate;
};

Q_LOGGER_EXPORT void qSetLoggingRules(const QByteArray &rules);
Q_LOGGER_EXPORT void qSetLoggingRulesFile(const QString &path);

QT_LOGGER_END_NAMESPACE
QT_END_HEADER

QT_LOGGER_USE_NAMESPACE

#if defined(qDebug)
#  undef qDebug
#endif

#define qDebug \
    for (bool enabled = QLoggingCategory::defaultCategory().isEnabled(QtDebugMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, QLoggingCategory::defaultCategory().categoryName()).debug

#if defined(qWarning)
#  undef qWarning
#endif

#define qWarning \
    for (bool enabled = QLoggingCategory::defaultCategory().isEnabled(QtWarningMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, QLoggingCategory::defaultCategory().categoryName()).warning

#if defined(qCritical)
#  undef qCritical
#endif

#define qCritical \
    for (bool enabled = QLoggingCategory::defaultCategory().isEnabled(QtCriticalMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, QLoggingCategory::defaultCategory().categoryName()).critical

//This marco creates the QLoggingCategory object in an empty namespace
//to prevent potential linker problems if someone else uses the same categorytype in another file.
#define QT_LOG_CATEGORY(categorytype, categoryname)                           \
    namespace {                                                               \
        static QLoggingCategory categorytype(categoryname);                   \
    }

#define qCDebug(category) \
    for (bool enabled = category.isEnabled(QtDebugMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, category.categoryName()).debug()

#define qCWarning(category) \
    for (bool enabled = category.isEnabled(QtWarningMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, category.categoryName()).warning()

#define qCCritical(category) \
    for (bool enabled = category.isEnabled(QtCriticalMsg); enabled; enabled = false) \
        QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO, category.categoryName()).critical()

#endif // QLOGGER_H
