/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the demonstration applications of the logger module
** of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QtCore/QMutexLocker>
#include <QtCore/QMutex>
#include <QtCore/QFile>
#include <QtCore/QThread>
#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtLogger/QtLogger>

QT_LOG_CATEGORY(My_Category_A, "My.Category.A")
QT_LOG_CATEGORY(My_Category_B, "My.Category.B")
QT_LOG_CATEGORY(My_Category_C, "My.Category.C")

#define LOGOUTPUTFILE "./logs.txt"
#define LOGRULESFILE "./logrules.txt"

//![1]
QT_LOG_CATEGORY(NOKIA_DRIVER_USB, "Nokia.driver.usb")
QT_LOG_CATEGORY(NOKIA_DRIVER_EVENT, "Nokia.driver.event")
//![1]

QMutex mutex;
QtMessageHandler oldMessageHandler;

QT_LOGGER_USE_NAMESPACE

//Messagehandler to write into a file
static void myCustomMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    //Lock against other threads accessing the file
    QMutexLocker locker(&mutex);
    QFile outFile(LOGOUTPUTFILE);
    if (outFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        QByteArray message;
        message.append("<");
        message.append(context.category);
        message.append(">");

        message.append(qPrintable(msg));
        message.append('\n');
        outFile.write(message);
    }
    oldMessageHandler(type, context, msg);
}

class LogThread : public QThread
{
public:
    LogThread() {}

protected:
    void run();
};

void LogThread::run()
{
//![4]
    QLoggingCategory myCategoryObject("My.Category.Object");
//![4]

    for (int i = 0; i < 60; i++) {
        qDebug() << "loop " << i;
//![3]
        //Category created by QT_LOG_CATEGORY
        qCDebug(NOKIA_DRIVER_USB) << "USB legacy loaded";

        //Category created by using QLoggingCategory directly
        qCDebug(myCategoryObject) << "Log with my category object";
//![3]
        sleep(1);
        qWarning() << "loop " << i;
        sleep(1);
        qCritical() << "loop " << i;
        sleep(1);
        qCDebug(My_Category_A) << "loop " << i;
        qCDebug(My_Category_B) << "loop " << i;
        qCDebug(My_Category_C) << "loop " << i;
        sleep(1);
        qCWarning(My_Category_A) << "loop " << i;
        qCWarning(My_Category_B) << "loop " << i;
        qCWarning(My_Category_C) << "loop " << i;
        sleep(1);
        qCCritical(My_Category_A) << "loop " << i;
        qCCritical(My_Category_B) << "loop " << i;
        qCCritical(My_Category_C) << "loop " << i;
        sleep(1);
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qputenv ("QT_MESSAGE_PATTERN", QString("%{category}: %{type},%{message}").toLatin1());
    oldMessageHandler = qInstallMessageHandler(myCustomMessageHandler);

    //delete old logoutput file
    QFile::remove(LOGOUTPUTFILE);

    fprintf(stdout, "%s\r\n", "Now you can open the logrules.txt file and de/activate categories");
    if (QFile::exists("./logrules.txt")){
//![2]
        QtLogger::qSetLoggingRulesFile("./logrules.txt");
//![2]
    }
    else {
//![5]
        QtLogger::qSetLoggingRules(QByteArray("Nokia.driver.usb=true"));
//![5]
    }

    LogThread logthread;
    logthread.start();

    return a.exec();
}
