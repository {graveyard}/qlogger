OTHER_FILES += $$PWD/logger.qdocconf

docs_target.target = docs
docs_target.commands = qdoc $$PWD/logger.qdocconf

QMAKE_EXTRA_TARGETS = docs_target
QMAKE_CLEAN += "-r $$PWD/html"

OTHER_FILES += $$PWD/src/*.qdoc
