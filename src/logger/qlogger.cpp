/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the logger module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QList>
#include <QByteArray>
#include <QString>
#include <QFile>
#include <QFileInfo>
#include <QStandardPaths>
#include <QBuffer>
#include <QFileSystemWatcher>
#include <QThread>
#include <QCoreApplication>
#include <QMap>
#include "qlogger_p.h"

QT_USE_NAMESPACE
QT_LOGGER_BEGIN_NAMESPACE

// qDebug truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | true
//  true       | false            | false
//  true       | true             | true

// qWarning truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | true
//  true       | false            | false
//  true       | true             | true

// qCritical truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | true
//  true       | false            | false
//  true       | true             | true

// qCDebug truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | false
//  true       | false            | false
//  true       | true             | true

// qCWarning truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | true
//  true       | false            | false
//  true       | true             | true

// qCCritical truth table
// ---------------------------------------
// Log enabled | Category enabled | do Log
// ---------------------------------------
//  false      | ignore           | true
//  true       | false            | false
//  true       | true             | true


Q_GLOBAL_STATIC(QLoggingPrivate, qLogging)

// This object is returned from an exported API so it lives longer than QLoggingPrivate
static class QLoggingCategoryDefault : public QLoggingCategory
{
public:
    QLoggingCategoryDefault()
        : QLoggingCategory("default")
    {
    }
} default_QLoggingCategory;

/*!
    \class QLoggingCategory
    \ingroup qlogging_classes

    \brief The QLoggingCategory class represents a category logging object for the category logging framework.

    Users can create a QLoggingCategory object and use it in conjunction with
    qCDebug, qCWarning and qCCritical.
*/

/*!
    Construct a QLoggingCategory object with the provided \a category name.
    The object becomes the local identifier for the category.
*/
QLoggingCategory::QLoggingCategory(const char *category)
    : d_ptr(0)
    , _categoryName(category)
{
}

/*!
    Returns the category name.
*/
const char* QLoggingCategory::categoryName()
{
    return _categoryName;
}

/*!
    \internal Returns the QLoggingCategory object used by the qDebug, qWarning and qCritical macros.
*/
QLoggingCategory& QLoggingCategory::defaultCategory()
{
    return default_QLoggingCategory;
}

/*!
    Destruct a QLoggingCategory object
*/
QLoggingCategory::~QLoggingCategory()
{
    if (!d_ptr) return;
    QLoggingPrivate *qlp = qLogging();
    if (!qlp) return; // logging system is gone
    qlp->releasePrivate(*this);
}

/*!
    Returns true if a message of type \a msgtype will be printed. Returns false otherwise.

    This function may be useful to avoid doing expensive work to generate data that is only used for debug output.

    \code
        // don't run the expensive code if the string won't print
        if (CAT.isEnabled(QtDebugMsg)) {
            QStringList items;
            foreach (obj, list) {
                items << format(obj);
            }
            qCDebug(CAT) << items;
        }
    \endcode

    Note that the expansion of qCDebug() prevents arguments from being evaluated if the string won't print so it is not normally required to check isEnabled().

    \code
        // expensive_func is not called if the string won't print
        qCDebug(CAT) << expensive_func();
    \endcode
*/
bool QLoggingCategory::isEnabled(QtMsgType msgtype)
{
    QLoggingPrivate *qlp = qLogging();

    // If the logging system is available, we might need to register our category object.
    if (qlp && qlp->registerCategories())
        return qLogging()->isEnabled(*this, msgtype);

    // If we're the default category or we were previously registered, we'll have cached values.
    if (d_ptr)
        return d_ptr->statusMessageType(msgtype);

    // We don't have cached values. Use the defaults.
    // NOTE qDebug/qWarning/qCritical never hit this (see default_QLoggingCategory)
    switch (msgtype) {
        case QtDebugMsg: return false;
        case QtWarningMsg: return true;
        case QtCriticalMsg: return true;
        default: break;
    }
    return false;
}

/*!
    \relates QLoggingCategory
    Load logging rules from \a path.

    If \a path is relative, QStandardPaths::writeableLocation(QStandardPaths::ConfigLocation) will be prepended.

    Note that if the QT_LOGGING_CONFIG environment variables points to a file, this function does nothing.
    \sa {Activate Logging Rules}
*/
void qSetLoggingRulesFile(const QString &path)
{
    QLoggingPrivate *qlp = qLogging();
    if (!qlp) return; // logging system is gone
    if (qlp->checkEnvironment()) return; // can't override the environment variable
    QString config = QLoggingPrivate::resolveConfigFile(path);
    if (!config.isEmpty())
        qlp->setLoggingRulesFile(config);
}

/*!
    \relates QLoggingCategory
    Set logging \a rules directly.

    This is primarily intended for applications that wish to provide runtime control of their
    logging rather than relying on the user providing a configuration file.

    Note that if the QT_LOGGING_CONFIG environment variables points to a file, this function does nothing.
    \sa {Activate Logging Rules}
*/
void qSetLoggingRules(const QByteArray &rules)
{
    QLoggingPrivate *qlp = qLogging();
    if (!qlp) return; // logging system is gone
    if (qlp->checkEnvironment()) return; // can't override the environment variable
    if (!rules.isEmpty())
        qlp->setLoggingRules(rules);
}

/*!
    \relates QLoggingCategory
    \macro qCDebug(cat)
    Works like qDebug() but using category object \a cat.
    Note: this does not process arguments if the string will not be printed so do not rely on side effects.
    \code
    qCDebug(CAT) << "my message";
    \endcode
    \sa QT_LOG_CATEGORY(), {Creating Logging Rules}, QLoggingCategory
*/

/*!
    \relates QLoggingCategory
    \macro qCWarning(cat)
    Works like qWarning() but using category object \a cat.
    Note: this does not process arguments if the string will not be printed so do not rely on side effects.
    \code
    qCWarning(CAT) << "my message";
    \endcode
    \sa QT_LOG_CATEGORY(), {Creating Logging Rules}, QLoggingCategory
*/

/*!
    \relates QLoggingCategory
    \macro qCCritical(cat)
    Works like qCritical() but using category object \a cat.
    Note: this does not process arguments if the string will not be printed so do not rely on side effects.
    \code
    qCCritical(CAT) << "my message";
    \endcode
    \sa QT_LOG_CATEGORY(), {Creating Logging Rules}, QLoggingCategory
*/

/*!
    \relates QLoggingCategory
    \macro QT_LOG_CATEGORY(cat, categoryname)
    Registers a logging category with local identifier \a cat and complete identifier \a categoryname.

    This macro must be used outside of a class or method.
    \sa {Create your Category Logging Object in your Project}, QLoggingCategory
*/








/*********************************
 *Private objects implementation
 *********************************/

/*!
    \internal For Autotest
 */
QLoggingPrivate *qtLoggerInstance()
{
    // If we're really unlucky, this will be null (during shutdown of the app)
    QLoggingPrivate *qlp = qLogging();
    if (!qlp) qFatal("Cannot call qtLoggerInstance() because qLogging() is 0");
    return qlp;
}

/*!
    \internal
    QLoggingPrivate constructor
 */
QLoggingPrivate::QLoggingPrivate()
    : QObject(0)
    , _configFileWatcher(0)
    , _environment(false)
    , _registerCategories(false)
{
    //Move object to the application thread
    if (QCoreApplication::instance())
        this->moveToThread(QCoreApplication::instance()->thread());

    //setup the default category
    categoryPrivate(default_QLoggingCategory)->_enabledDebug = true;
    _registeredCategories.append(&default_QLoggingCategory);

    QByteArray ba = qgetenv("QT_LOGGING_CONFIG");
    if (!ba.isEmpty()) {
        QString path = QString::fromLocal8Bit(ba);
        QString config = QLoggingPrivate::resolveConfigFile(path);
        if (!config.isEmpty()) {
            _environment = true;
            setLoggingRulesFile(config);
        }
    }
}

/*!
    \internal
    QLoggingPrivate destructor
 */
QLoggingPrivate::~QLoggingPrivate()
{
}

/*!
    \internal
    Returns the resolved version of \a path.
*/
QString QLoggingPrivate::resolveConfigFile(const QString &path)
{
    QFileInfo fi(path);
    if (fi.fileName() == path)
        fi.setFile(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QLatin1Char('/') + path);
    QString config = fi.absoluteFilePath();
    if (QFile::exists(config))
        return config;
    return QString();
}

/*!
    \internal
    Function to set the logging config file.
    This function set the new QLogging configuration config file.
    If QT_LOGGING_CONFIG is set this function will do nothing.
*/
void QLoggingPrivate::setLoggingRulesFile(const QString &path)
{
    _registerCategories = false;
    _configFile = path;

    //Create filewatcher only if a config file exists
    if (!_configFileWatcher) {
        //This function can be called from a different thread.
        //The creation of the filewatcher needs to be in the application thread.
        //So we invoke the function that creates the filewatcher
        QMetaObject::invokeMethod(this, "createFileWatcher");
    }

    QFile cfgfile(_configFile);
    readSettings(cfgfile);
}

/*!
    \internal
    Invokable function to create the System File Watcher on the applicationthread.
*/
void QLoggingPrivate::createFileWatcher()
{
    _configFileWatcher = new QFileSystemWatcher(this);
    connect(_configFileWatcher, SIGNAL(fileChanged(QString)), SLOT(fileChanged(QString)));
    QStringList oldfiles = _configFileWatcher->files();
    if (!oldfiles.isEmpty())
        _configFileWatcher->removePaths(oldfiles);
    _configFileWatcher->addPath(_configFile);
}

/*!
    \internal
    Function to set the logging rules.
    This function set the new logging configuration.
    If QT_LOGGING_CONFIG environment vaiable is in use, this function will not make any changes.
*/
void QLoggingPrivate::setLoggingRules(const QByteArray &rules)
{
    _registerCategories = false;

    //Disable file watcher
    if (_configFileWatcher) {
        delete _configFileWatcher;
        _configFileWatcher = 0;
    }

    QBuffer buffer;
    buffer.setData(rules);
    readSettings(buffer);
}

/*!
    \internal
    Slot for filewatcher
*/
void QLoggingPrivate::fileChanged(const QString &path)
{
    //check if the logging rule file was changed
    if (path == _configFile){
        QFile cfgfile(_configFile);
        readSettings(cfgfile);
        _configFileWatcher->addPath(path);
    }
}

/*!
    \internal
    Reads the configuration out from a io device.
*/
void QLoggingPrivate::readSettings(QIODevice &device)
{
    QMutexLocker locker(&_mutexRegisteredCategory);
    {
        _logConfigItemList.clear();

        if (device.open(QIODevice::ReadOnly)) {
            QByteArray truearray("true");
            QByteArray line;
            while (!device.atEnd()) {
                //Simplify the string before creating pair
                line = device.readLine().replace(" ", "");
                line = line.simplified();
                const QList<QByteArray> pair = line.split('=');
                if (pair.count() == 2)
                    _logConfigItemList.append(QLogConfigFilterItem(QString::fromLatin1(pair.at(0))
                                                     , (pair.at(1).toLower() == truearray)));
            }
        }

        //Now all the categories are read, so we can update all known QLoggingCategories members.
        foreach (QLoggingCategory *category, _registeredCategories) {
            updateCategory(category);
        }

        _registerCategories = true;
    }
    emit configurationChanged();
}

/*!
    \internal
    Updates all the registered category members against the filter.
*/
void QLoggingPrivate::updateCategory(QLoggingCategory *log)
{
    QLoggingCategoryPrivate *d_ptr = categoryPrivate(*log);
    //set the default back (debug disable, warning and critical enabled)
    if (log == &QLoggingCategory::defaultCategory()) {
        d_ptr->_enabledDebug = true;
        d_ptr->_enabledWarning = true;
        d_ptr->_enabledCritical = true;

    } else {
        d_ptr->_enabledDebug = false;
        d_ptr->_enabledWarning = true;
        d_ptr->_enabledCritical = true;
    }

    foreach (QLogConfigFilterItem item, _logConfigItemList) {
        //Debug
        int filterpass = item.pass(log, QtDebugMsg);
        //apply filter if filterpass is not 0
        if (filterpass != 0)
            d_ptr->_enabledDebug = (filterpass > 0);
        //Warning
        filterpass = item.pass(log, QtWarningMsg);
        if (filterpass != 0)
            d_ptr->_enabledWarning = (filterpass > 0);
        //Critical
        filterpass = item.pass(log, QtCriticalMsg);
        if (filterpass != 0)
            d_ptr->_enabledCritical = (filterpass > 0);
    }
}

/*!
    \internal
    Function that checks if the category is enabled and registered the category member if not already registered.
*/
bool QLoggingPrivate::isEnabled(QLoggingCategory &category, QtMsgType type)
{
    QLoggingCategoryPrivate *d_ptr = categoryPrivate(category);
    if (d_ptr->_registered)
        return d_ptr->statusMessageType(type);

    //category is unregistered.
    //First update category (let it through the filter)
    {
        QMutexLocker locker(&_mutexRegisteredCategory);
        //lock against _logConfigItemList between updateCategory and readSettings
        updateCategory(&category);
        d_ptr->_registered = true;
       _registeredCategories.append(&category);
    }

    return d_ptr->statusMessageType(type);
}

/*!
    \internal
    Unregister a category object.
*/
void QLoggingPrivate::unregisterCategory(QLoggingCategory &category)
{
    QMutexLocker locker(&_mutexRegisteredCategory);
    //lock against _logConfigItemList between updateCategory and readSettings
    categoryPrivate(category)->_registered = false;
    _registeredCategories.removeOne(&category);
}

/*!
    \internal
    Returns the private object for \a cat
*/
QLoggingCategoryPrivate *QLoggingPrivate::categoryPrivate(QLoggingCategory &cat)
{
    if (!cat.d_ptr) {
        QMutexLocker locker(&_privateCategoryObjectsMutex);
        //Another thread can call this function for the same QLoggingCategory object now
        //Check the d_ptr after mutex lock again.
        if (!cat.d_ptr) {
            QString strcategory;
            //just for the insane case someone calls this constructor with an empty category parameter
            if (cat._categoryName)
                strcategory = QString::fromLatin1(cat._categoryName);
            QMap<QString, QLoggingCategoryPrivate* >::iterator it = _privateCategoryObjects.find(strcategory);
            if (it != _privateCategoryObjects.end())
                cat.d_ptr = *it;
            else {
                cat.d_ptr = new QLoggingCategoryPrivate;
                _privateCategoryObjects.insert(strcategory, cat.d_ptr);
            }
            cat.d_ptr->_references++;
        }
    }
    return cat.d_ptr;
}

/*!
    \internal
    Releases the private object for \a cat
*/
void QLoggingPrivate::releasePrivate(QLoggingCategory &cat)
{
    QMutexLocker locker1(&_privateCategoryObjectsMutex);
    cat.d_ptr->_references--;
    if (cat.d_ptr->_references == 0) {
        if (cat.d_ptr->_registered)
            unregisterCategory(cat);
        QString strcategory = QString::fromLatin1(cat._categoryName);
        QMap<QString, QLoggingCategoryPrivate* >::iterator it = _privateCategoryObjects.find(strcategory);
        if (it != _privateCategoryObjects.end())
            _privateCategoryObjects.remove(strcategory);

        delete cat.d_ptr;
    }
}

/*!
    \fn QLoggingPrivate::checkEnvironment()
    \internal
    Returns true if the environment variable is found.
    The first time this is called, the logging rules file pointed to by the
    environment variable will be processed.
*/

/*!
    \fn QLoggingPrivate::regsterCategories()
    \internal
    Returns true if category objects should be registered.
    This is primarily used as an optimization to avoid registering
    category objects if no logging config has been specified.
*/

/*!
    \internal Constructor of the private QLoggingCategory object
*/
QLoggingCategoryPrivate::QLoggingCategoryPrivate()
    : _enabledDebug(false)
    , _enabledWarning(true)
    , _enabledCritical(true)
    , _registered(false)
    , _references(0)
{
}

QLoggingCategoryPrivate::~QLoggingCategoryPrivate()
{
}

/*!
    \internal Returns true if the message type is activated otherwise false;
*/
bool QLoggingCategoryPrivate::statusMessageType(const QtMsgType &type)
{
    switch (type) {
        case QtDebugMsg: return _enabledDebug;
        case QtWarningMsg: return _enabledWarning;
        case QtCriticalMsg: return _enabledCritical;
        default:
            break;
    }
    return false;
}

#define INVALID 0x00
#define CATEGORY 0x01
#define LEFTFILTER 0x02
#define RIGHTFILTER 0x04
#define MIDFILTER 0x06
/*!
    \internal
    Constructor of a filter item.
*/
QLogConfigFilterItem::QLogConfigFilterItem(const QString &category, bool active)
    : _category(category)
    , _active(active)
{
    parse();
}

/*!
    \internal
    Parses the category and check witch kind of wildcard the filter can contain.
    Allowed is f.e.g.:
             com.Nokia.*     LEFTFILTER
             *.Nokia         RIGHTFILTER
             *.Nokia*        MIDFILTER
 */
void QLogConfigFilterItem::parse()
{
    _type = INVALID;
    int index = _category.indexOf(QString::fromLatin1("*"));
    if (index < 0)
        _type |= CATEGORY;
    else {
        if (index == 0) {
            _type |= RIGHTFILTER;
            _category = _category.remove(0, 1);
            index = _category.indexOf(QString::fromLatin1("*"));
        }
        if (index == (_category.length() - 1)) {
            _type |= LEFTFILTER;
            _category = _category.remove(_category.length() - 1, 1);
        }
    }
}

/*!
    \internal
    return value 1 means filter passed, 0 means filter doesn't influence this category, -1 means category doesn't pass this filter
 */
int QLogConfigFilterItem::pass(QLoggingCategory *log, const QtMsgType &type)
{
    QString fullCategory = QString::fromLatin1(log->categoryName());
    switch (type) {
        case QtDebugMsg:
            fullCategory += QString::fromLatin1(".debug");
        break;
        case QtWarningMsg:
            fullCategory += QString::fromLatin1(".warning");
        break;
        case QtCriticalMsg:
            fullCategory += QString::fromLatin1(".critical");
        break;
        default:
            break;
    }

    if (_type == CATEGORY) {
        //Can be
        //NOKIA.com.debug = true
        //or
        //NOKIA.com = true
        if (_category == QString::fromLatin1(log->categoryName()) || _category == fullCategory)
            return (_active ? 1 : -1);
    }

    int idx = 0;
    if (_type == MIDFILTER) {
        //e.g. *.Nokia*
        idx = fullCategory.indexOf(_category);
        if (idx >= 0)
            return (_active ? 1 : -1);
    } else {
        idx = fullCategory.indexOf(_category);
        if (_type == LEFTFILTER) {
            //e.g. com.Nokia.*
            if (idx == 0)
                return (_active ? 1 : -1);
        } else if (_type == RIGHTFILTER) {
            //e.g. *.Nokia
            if (idx == (fullCategory.count() - _category.count()))
                return (_active ? 1 : -1);
        }
    }
    return 0;
}

#include "moc_qlogger_p.cpp"

QT_LOGGER_END_NAMESPACE


