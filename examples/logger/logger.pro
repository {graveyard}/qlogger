TARGET = logger
TEMPLATE = app
QT += core logger
QT -= gui
CONFIG += console
CONFIG -= app_bundle
SOURCES = main.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/qtbase/logger
INSTALLS += target
data.files = logrules.txt
data.path = $$[QT_INSTALL_EXAMPLES]/qtbase/logger
INSTALLS += data

!equals(PWD, $${OUT_PWD}) {
  #shadow build
  QMAKE_SUBSTITUTES += copy
  copy.input = $$PWD/logrules.txt
  copy.output = $$OUT_PWD/logrules.txt
}
