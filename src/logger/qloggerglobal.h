/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the logger module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QLOGGERGLOBAL_H
#define QLOGGERGLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QT_NAMESPACE)
#  define QT_LOGGER_PREPEND_NAMESPACE(name) ::QT_NAMESPACE::QtLogger::name
#  define QT_LOGGER_BEGIN_NAMESPACE namespace QT_NAMESPACE { namespace QtLogger {
#  define QT_LOGGER_END_NAMESPACE } }
#  define QT_LOGGER_USE_NAMESPACE using namespace QT_NAMESPACE; using namespace QtLogger;
#else
#  define QT_LOGGER_PREPEND_NAMESPACE(name) ::QtLogger::name
#  define QT_LOGGER_BEGIN_NAMESPACE namespace QtLogger {
#  define QT_LOGGER_END_NAMESPACE }
#  define QT_LOGGER_USE_NAMESPACE using namespace QtLogger;
#endif

#if !defined(Q_LOGGER_EXPORT)
#  if defined(QT_SHARED)
#    define Q_LOGGER_EXPORT Q_DECL_EXPORT
#  else
#    define Q_LOGGER_EXPORT
#  endif
#endif

#define QT_LOGGER_VERSION_NAME "org.qt-project.Qt.logger.api.version"
#define QT_LOGGER_IMPLEMENTATION_VERSION_NAME "org.qt-project.Qt.logger.implementation.version"
#define QT_LOGGER_VERSION 1

#endif
