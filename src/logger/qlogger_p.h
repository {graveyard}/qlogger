/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the QtLogger module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QLOGGING_P_H
#define QLOGGING_P_H


#include <QList>
#include <QMutex>
#include <QString>
#include <QObject>
#include <QIODevice>
#include "qlogger.h"

QT_BEGIN_HEADER

QT_BEGIN_NAMESPACE
class QFileSystemWatcher;
QT_END_NAMESPACE

QT_LOGGER_BEGIN_NAMESPACE

class QLogConfigFilterItem
{
public:
    QLogConfigFilterItem(const QString &category, bool active);
    int pass(QLoggingCategory *log, const QtMsgType &type);
    void parse();
    int _type;
    QString _category;
    bool _active;
};

class QLoggingPrivate  : public QObject
{
    Q_OBJECT
public:
    QLoggingPrivate();
    virtual ~QLoggingPrivate();

    static QString resolveConfigFile(const QString &path);
    void setLoggingRulesFile(const QString &path);
    void setLoggingRules(const QByteArray &configcontent);
    bool isEnabled(QLoggingCategory &category, QtMsgType type);
    void unregisterCategory(QLoggingCategory &category);
    bool checkEnvironment() { return _environment; }
    bool registerCategories() { return _registerCategories; }
    QLoggingCategoryPrivate *categoryPrivate(QLoggingCategory &category);
    void releasePrivate(QLoggingCategory &category);

    Q_INVOKABLE void createFileWatcher();

    void readSettings(QIODevice &device);
    void updateCategory(QLoggingCategory *log);

Q_SIGNALS:
    void configurationChanged();

public slots:
    void fileChanged(const QString &path);

public:
    QFileSystemWatcher *_configFileWatcher;
    QList<QLoggingCategory *> _registeredCategories;
    QString _configFile;
    QMutex _mutexRegisteredCategory;
    QList<QLogConfigFilterItem> _logConfigItemList;
    bool _environment;
    bool _registerCategories;
    QMutex _privateCategoryObjectsMutex;
    QMap<QString, QLoggingCategoryPrivate *> _privateCategoryObjects;
};

class QLoggingCategoryPrivate
{
public:
    QLoggingCategoryPrivate();
    virtual ~QLoggingCategoryPrivate();
    bool statusMessageType(const QtMsgType &type);
    bool _enabledDebug;
    bool _enabledWarning;
    bool _enabledCritical;
    bool _registered;
    int _references;
};

Q_LOGGER_EXPORT QLoggingPrivate *qtLoggerInstance();


QT_LOGGER_END_NAMESPACE
QT_END_HEADER

#endif //QLOGGING_P_H
