TEMPLATE = subdirs

SUBDIRS += module_qlogger_src
module_qlogger_src.subdir = src
module_qlogger_src.target = module-qlogger-src

SUBDIRS += module_qlogger_tests
module_qlogger_tests.subdir = tests
module_qlogger_tests.target = module-qlogger-tests
module_qlogger_tests.depends = module_qlogger_src
module_qlogger_tests.CONFIG = no_default_install
!contains(QT_BUILD_PARTS,tests) {
    module_qlogger_tests.CONFIG += no_default_target
}

SUBDIRS += module_qlogger_examples
module_qlogger_examples.subdir = examples
module_qlogger_examples.target = module-qlogger-examples
module_qlogger_examples.depends = module_qlogger_src
!contains(QT_BUILD_PARTS,examples) {
    module_qlogger_examples.CONFIG = no_default_target no_default_install
}

include(doc/doc.pri)
